`timescale 1ns / 1ps
module Create_TB;
	reg RdBF, Init, creaenhe, Reset, Clk; 
	reg [6:0] D; 		
	wire BF, RS, RW;
	inout MsbOD;
	wire [6:0] Out_display;

	
	Create uut (.RdBF(RdBF), .Init(Init), .creaenhe(creaenhe), .D(D), .Reset(Reset), .BF(BF), .RS(RS), .RW(RW), .Out_display(Out_display), .MsbOD(MsbOD), .Clk(Clk));
	
initial begin	
		RdBF=1;
		Init=0;
		creaenhe=0;
		Reset=0;	
		Clk=0;
		D=7'b1010101;
end
initial begin
		forever begin
		#30;
		Clk=0;
		#30;
		Clk=1;
		end
end
initial begin
		forever begin
		#60;
		Init=0;
		#60;
		Init=0;
		#1200
		Init=1;
		#60
		Init=0;		
		end
end

initial begin
		forever begin
		#180;
		RdBF=0;
		#60;
		RdBF=0;
		end
end

initial begin
		forever begin
		#60;
		creaenhe=0;
		#60;
		creaenhe=1;
		#60
		creaenhe=0;
		#600
		creaenhe=0;		
		end
end

initial begin
		forever begin
		#60;
		Reset=0;
		#60;
		Reset=0;
		#2500
		Reset=1;
		#60
		Reset=0;		
		end
end   

     initial begin: TEST_CASE
                $dumpfile("Create_TB.vcd");
                $dumpvars(-1, uut);
                #10000 $finish;
        end

endmodule
