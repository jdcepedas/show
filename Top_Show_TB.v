`timescale 1ns / 1ps

module Top_Show_TB;

	reg [3:0] sw;
	reg [1:0] btn;
	reg clk;       
        wire [6:0] StringDisplay;
        wire RS, RW, EN;
        inout MsbOD;
	

   Top_Show uut (.clk(clk), .sw(sw), .btn(btn), .RS(RS), .MsbOD(MsbOD), .RW(RW), .EN(EN));


   initial begin  
      clk = 0; btn=2'b0; sw=4'b0; 
   end 

initial  begin  // Process for clk
	forever begin
	#1
	clk=0;
	#1
	clk=1;
	end
end

initial begin
		#51000001
		sw='hA;
		btn[0]=1;
		#600
		btn[0]=0;
end

initial begin: TEST_CASE
	$dumpfile("Top_Show_TB.vcd");
	$dumpvars(-1, uut);
	#90000000 $finish;
end

endmodule

